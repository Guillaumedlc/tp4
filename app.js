var pizza=0;
var pate=0;
var nbparts=1;
var extra=0;

$(function(){
	$(".type.pizza-type label").hover(
  				function() {
    				$(this).find("span").css("display","inline");
  				}, 
  				function() {
    				$(this).find("span").css("display","none");
			  	}
	);
	$(".type.nb-parts input").on('keyup',
		function(){
			nbparts=$('.type.nb-parts input').val();
			var q = nbparts / 6;
			var r = nbparts % 6;
			$(".pizza-pict").remove();
			if(q >= 1)
				for(var i=1;i<=q;i++)
					$(".type.nb-parts").append('<span class="pizza-6 pizza-pict"></span>');
			if(r == 1)
				$(".type.nb-parts").append('<span class="pizza-1 pizza-pict"></span>');
			else if(r == 2)
				$(".type.nb-parts").append('<span class="pizza-2 pizza-pict"></span>');
			else if(r == 3)
				$(".type.nb-parts").append('<span class="pizza-3 pizza-pict"></span>');
			else if(r == 4)
				$(".type.nb-parts").append('<span class="pizza-4 pizza-pict"></span>');
			else if(r == 5)
				$(".type.nb-parts").append('<span class="pizza-5 pizza-pict"></span>');
			affichage();
		}
	);
	$(".btn.btn-success.next-step").click(
		function(){
		var n = $(".type.pizza-type input:checked" ).length;
		if(n > 0){
			$(".infos-client.no-display").css("display","inline");
			$(".btn.btn-success.next-step").css("display","none");
		}else
			alert("Veuillez Selectionner une Pizza");
		}
	);
	$(".btn.btn-default.add").click(
		function(){
			$(".btn-default").parent().prepend('<br><input type="text"/>');
		}
	);
	$(".done").click(
		function(){
			var prenom = $('.infos-client .type:first-child input').val();
			$(".typography-row,.stick-right").remove();
			$(".container").append('<span class="text-note"> Merci '+ prenom +'. Votre commande sera livrée dans 15 minutes</span>');
		}
	)
	$(".type.pizza-type label").click(
		function(){
			pizza=$("input[name='type']:checked").data("price");
			console.log("prix pizza="+pizza);
			affichage();
		}
	);
	$("input[name='pate']").click(
		function(){
			pate=$("input[name='pate']:checked").data("price");
			console.log("pate="+pate);
			affichage();
		}
	);
	$("input[name='extra']").click(
		function(){
			var extra1,extra2,extra3,extra4;
			if($("input[name='extra'][value='1']").is(':checked')){
				extra1=$("input[name='extra'][value='1']").data("price");
			}else{
				extra1=0;
			}
			if($("input[name='extra'][value='2']").is(':checked')){
				extra2=$("input[name='extra'][value='2']").data("price");
			}else{
				extra2=0;
			}
			if($("input[name='extra'][value='3']").is(':checked')){
				extra3=$("input[name='extra'][value='3']").data("price");
			}else{
				extra3=0;
			}
			if($("input[name='extra'][value='4']").is(':checked')){
				extra4=$("input[name='extra'][value='4']").data("price");
			}else{
				extra4=0;
			}
			extra=extra1+extra2+extra3+extra4;
			console.log("extra="+extra);
			affichage();
		}
	)
})

function affichage(){
	var prix=(nbparts/6)*(pizza+pate+extra);
	$(".stick-right .tile p").replaceWith("<p>"+prix+"€</p>");
}